# -*- coding: utf-8 -*-
"""
This is the main entry point for the "dispatch-reporting-batch" AWS Lambda Function.
Start here when attempting to grok the Lambda code.
"""
import logging
import os

from http import HTTPStatus
from typing import Dict

import boto3
from botocore.exceptions import ClientError
import requests

logger = logging.getLogger(__name__)
logger.setLevel('INFO')

dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table(os.getenv('DYNAMO_TABLE', ''))

GREETING_ACTION = 'Welcome'
GREETING_SERVICE = 'GreeterService'
TEST_ESTABLISHMENT = u'testEstablishment'
TEST_CLIENT = u'testClient'
TEST_EMPLOYEE = u'testEmployee'
DEFAULT_GREETING_MSG = u'Welcome to a sample service!'
NON_GREETING_MSG = u'Sorry, can\'t recognize you.'


def get_greeting_msg():
    msg = DEFAULT_GREETING_MSG
    try:
        msg_query_result = table.get_item(
            Key={
                'PK': 'greeting',
                'SK': 'english'
            }
        )
    except ClientError as error:
        logger.error(f'Greeting message query failed, error: {error}')
        return DEFAULT_GREETING_MSG
    if 'Item' not in msg_query_result:
        logger.warning('No greeting message found in database')
        return DEFAULT_GREETING_MSG
    msg = msg_query_result['Item'].get('Greeting', msg)
    return msg


def handle_response(status_code: int, message: str) -> Dict[str, str]:
    return {'statusCode': status_code, 'body': message}


def handler(event: Dict[str, any], context: Dict[str, any]) -> Dict[str, any]:
    """
    The entry point for AWS Lambda Function 'dispatch-reporting-batch'

    Args:
        event: An event object. https://docs.aws.amazon.com/lambda/latest/dg/python-programming-model-handler-types.html
        context: AWS Lambda context object. https://docs.aws.amazon.com/lambda/latest/dg/python-context-object.html

    Returns: Greeting message if User is authorized
    """
    access_token = event.get('headers', {}).get('Authorization', None)
    if not access_token:
        logger.error(u'JWT token should be included in the Authorization header')
        return handle_response(HTTPStatus.UNAUTHORIZED, NON_GREETING_MSG)
    authz_url = os.getenv('AUTHZ_SERVICE_URL')
    if not authz_url:
        logger.error(u'AUTHZ_SERVICE_URL environment variable is not set')
        return handle_response(HTTPStatus.INTERNAL_SERVER_ERROR, HTTPStatus.INTERNAL_SERVER_ERROR.description)
    headers = {'Authorization': access_token}
    payload = {
        'action': GREETING_ACTION,
        'service': GREETING_SERVICE,
        'establishment': TEST_ESTABLISHMENT,
        'clientId': TEST_CLIENT,
        'employeeId': TEST_EMPLOYEE,
    }
    authz_response = requests.get(os.getenv('AUTHZ_SERVICE_URL', ''),
                                  headers=headers, params=payload)
    logger.info(f'authz_response: {authz_response}')
    if authz_response.status_code != HTTPStatus.OK:
        authz_response_msg = authz_response.json()
        if isinstance(authz_response_msg, dict):
            authz_response_msg = authz_response_msg.get('message', '')
        logger.error(f'Received AuthZ error status code: {authz_response.status_code} '
                     f'response: {authz_response_msg}')
        return handle_response(authz_response.status_code, authz_response_msg)
    return handle_response(authz_response.status_code, get_greeting_msg())
