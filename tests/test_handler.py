from http import HTTPStatus
import unittest
from unittest.mock import Mock, patch
import os

from botocore.exceptions import ClientError

from dispatch-reporting-batch import handler


AUTHZ_AUTHORIZED_MESSAGE = 'Authorized'
AUTHZ_UNAUTHORIZED_MESSAGE = 'Unauthorized'


class HandlerTestCase(unittest.TestCase):
    def setUp(self):
        self.authz_url = os.getenv('AUTHZ_SERVICE_URL')
        self.mock_event = {
            'headers': {
                'Authorization': 'Bearer xxxxx.yyyyy.zzzzz'
            }
        }
        self.params = {
            'action': handler.GREETING_ACTION,
            'service': handler.GREETING_SERVICE,
            'establishment': handler.TEST_ESTABLISHMENT,
            'clientId': handler.TEST_CLIENT,
            'employeeId': handler.TEST_EMPLOYEE,
        }
        self.db_greeting_msg = u'Test Greeting'
        handler.table = Mock(get_item=Mock(return_value={
            'Item': {'Greeting': self.db_greeting_msg}
        }))

    def _mock_response(self, status=None, json_data=None):
        mock_resp = Mock()
        mock_resp.status_code = status
        mock_resp.json = Mock(return_value=json_data)
        return mock_resp

    @patch('requests.get')
    def test_handler_when_user_not_authenticated__returns_unauthorized(self, mock_get):
        self.mock_event['headers'] = {}
        result = handler.handler(self.mock_event, Mock())
        mock_get.assert_not_called
        self.assertEqual(result, {'statusCode': HTTPStatus.UNAUTHORIZED, 'body': handler.NON_GREETING_MSG})

    @patch('os.getenv')
    @patch('requests.get')
    def test_handler_when_no_authz_url__returns_error(self, mock_get, mock_getenv):
        mock_getenv.return_value = None
        result = handler.handler(self.mock_event, Mock())
        mock_get.assert_not_called
        self.assertEqual(result, {'statusCode': HTTPStatus.INTERNAL_SERVER_ERROR,
                                  'body': HTTPStatus.INTERNAL_SERVER_ERROR.description})

    @patch('requests.get')
    def test_handler_when_user_authorized__returns_success(self, mock_get):
        mock_resp = self._mock_response(status=HTTPStatus.OK,
                                        json_data={'message': AUTHZ_AUTHORIZED_MESSAGE})
        mock_get.return_value = mock_resp
        result = handler.handler(self.mock_event, Mock())
        mock_get.assert_called_once()
        mock_get.assert_called_with(self.authz_url, headers=self.mock_event['headers'], params=self.params)
        self.assertEqual(result, {'statusCode': HTTPStatus.OK, 'body': self.db_greeting_msg})

    @patch('requests.get')
    def test_handler_when_user_not_authorized__returns_forbidden(self, mock_get):
        mock_resp = self._mock_response(status=HTTPStatus.FORBIDDEN,
                                        json_data={'message': AUTHZ_UNAUTHORIZED_MESSAGE})
        mock_get.return_value = mock_resp
        result = handler.handler(self.mock_event, Mock())
        mock_get.assert_called_once()
        mock_get.assert_called_with(self.authz_url, headers=self.mock_event['headers'], params=self.params)
        self.assertEqual(result, {'statusCode': HTTPStatus.FORBIDDEN, 'body': AUTHZ_UNAUTHORIZED_MESSAGE})

    @patch('requests.get')
    def test_handler_when_db_error__returns_success_with_default_msg(self, mock_get):
        mock_query = Mock()
        mock_query.side_effect = ClientError({
            'Error': {}
        }, 'mock_get_item')
        handler.table = Mock(get_item=mock_query)
        mock_resp = self._mock_response(status=HTTPStatus.OK,
                                        json_data={'message': AUTHZ_AUTHORIZED_MESSAGE})
        mock_get.return_value = mock_resp
        result = handler.handler(self.mock_event, Mock())
        mock_get.assert_called_once()
        mock_get.assert_called_with(self.authz_url, headers=self.mock_event['headers'], params=self.params)
        self.assertEqual(result, {'statusCode': HTTPStatus.OK, 'body': handler.DEFAULT_GREETING_MSG})


if __name__ == '__main__':
    unittest.main()
